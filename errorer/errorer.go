package errorer

import "fmt"

const (
	ProxyError       = 100
	InterpreterError = 200
	SetupError       = 300
	StopAction       = 401
	ReloadAction     = 402
)

type Errorer struct {
	Code int
	Msg  string
	Err  error
}

func (errer *Errorer) Error() string {
	if errer.Err != nil {
		return fmt.Sprintf("code: %d\nmsg: %s\ninternal error: %v", errer.Code, errer.Msg, errer.Err)
	}
	return fmt.Sprintf("code: %d\nmsg: %s\n", errer.Code, errer.Msg)
}
