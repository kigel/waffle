package proxy

import (
	"net/http"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
)

func (prx *Proxy) checkBlacklisted(req *http.Request) (bool, error) {
	// get the ip
	addrParts := strings.Split(req.RemoteAddr, ":")
	port := addrParts[len(addrParts)-1]

	// remove the port from the address
	ip := strings.Replace(req.RemoteAddr, ":"+port, "", -1)
	// remove brackets if ipv6
	ip = strings.Replace(ip, "[", "", -1)
	ip = strings.Replace(ip, "]", "", -1)

	data, err := prx.MongoManager.Get("data", "blacklist", bson.M{"ip": ip})
	if err != nil {
		return false, err
	}

	if len(data) > 0 {
		return true, nil
	}

	return false, nil
}
