package proxy

import (
	"net/http"

	"gitlab.com/kigel/waffle/errorer"
)

func (prx *Proxy) rootHandler() http.HandlerFunc {
	return func(writer http.ResponseWriter, req *http.Request) {
		// check if blacklisted
		isBlacklisted, err := prx.checkBlacklisted(req)
		if err != nil {
			prx.ErrCh <- errorer.Errorer{Code: errorer.ProxyError, Msg: "", Err: err}
			return
		}

		// if blacklisted just send them an error page
		if isBlacklisted {
			http.Error(writer, "Forbidden", http.StatusForbidden)
			return
		}

		// Send request to app and receive a response
		appRes, err := prx.sendReqApp(req)
		if err != nil {
			prx.ErrCh <- errorer.Errorer{Code: errorer.ProxyError, Msg: "", Err: err}
			return
		}

		// Send the response to the client
		err = prx.sendResClient(appRes, writer)
		if err != nil {
			prx.ErrCh <- errorer.Errorer{Code: errorer.ProxyError, Msg: "", Err: err}
			return
		}
	}
}
