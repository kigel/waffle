package settings

import (
	"os"

	"gopkg.in/yaml.v3"
)

type Settings struct {
	Proxy struct {
		App_Adrress string `yaml:"app_address"`
		Out_Adrress string `yaml:"out_address"`
		Ssl_Crt     string `yaml:"ssl_crt"`
		Ssl_Key     string `yaml:"ssl_key"`
	}
	Database struct {
		Db_Adrress string `yaml:"db_adrress"`
	}
}

func GetSettings(settingsPath string) (*Settings, error) {
	settingsRaw, err := os.ReadFile(settingsPath)
	if err != nil {
		return nil, err
	}

	var sett Settings
	if err = yaml.Unmarshal(settingsRaw, &sett); err != nil {
		return nil, err
	}

	return &sett, nil
}
