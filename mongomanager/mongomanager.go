package mongomanager

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/kigel/waffle/settings"
)

type MongoManager struct {
	Settings *settings.Settings
	Client   *mongo.Client
}

func (mngman *MongoManager) Connect() error {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://"+mngman.Settings.Database.Db_Adrress))
	if err != nil {
		return err
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		client.Disconnect(ctx)
		return err
	}

	// put the client into the struct
	mngman.Client = client

	return nil
}

func (mngman *MongoManager) Disconnect() error {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	err := mngman.Client.Disconnect(ctx)
	if err != nil {
		return err
	}

	return nil
}

func (mngman *MongoManager) Get(db string, coll string, filter interface{}) ([]bson.M, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	cur, err := mngman.Client.Database(db).Collection(coll).Find(ctx, filter)
	if err != nil {
		return nil, err
	}

	// check if documents were found
	try := cur.TryNext(ctx)
	if !try {
		return []bson.M{}, nil
	}

	var dataSlice []bson.M
	var tempData bson.M

	// setup the returned slice
	for {
		err = cur.Decode(&tempData)
		if err != nil {
			return nil, err
		}
		dataSlice = append(dataSlice, tempData)

		if !cur.Next(ctx) {
			break
		}
	}

	return dataSlice, nil
}

func (mngman *MongoManager) Insert(db string, coll string, data interface{}) error {
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	_, err := mngman.Client.Database(db).Collection(coll).InsertOne(ctx, data)
	if err != nil {
		return err
	}

	return nil
}
